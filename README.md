# CI/CD Pipeline Documentation

This documentation provides detailed instructions for setting up and managing a Continuous Integration and Continuous Deployment (CI/CD) pipeline for a simple standalone application using GitLab and Jenkins.


## Installation Steps

### GitLab:

1. **Sign Up/Log In**: Go to [GitLab](https://gitlab.com/) and sign up for an account or log in if you already have one.
2. **Create New Project**: Create a new project/repository named "SimpleApp".
3. **Initialize Repository**: Initialize the repository with a README.md file.

### Jenkins:

1. **Download Jenkins**: Download Jenkins from [https://www.jenkins.io/download/](https://www.jenkins.io/download/).
2. **Install Jenkins**: Follow the installation instructions for your operating system.
3. **Access Jenkins**: Once installed, access Jenkins through your web browser.

## Configuring Jenkins to Integrate with GitLab

1. **Install GitLab Plugin**: In Jenkins, install the "GitLab Plugin" through the Plugin Manager.
2. **Configure GitLab Connection**: Go to Jenkins > Manage Jenkins > Configure System. Under the "GitLab" section, configure the GitLab connection by providing the GitLab host URL and API token.

## Creating and Configuring Jenkins Pipeline

1. **Create Pipeline Project**: In Jenkins, create a new Pipeline project.
2. **Configure Pipeline**: Integrate the pipeline with your GitLab repository by specifying the repository URL and credentials if needed.
3. **Create Jenkinsfile**: Create a Jenkinsfile in the root of your GitLab repository.
4. **Define Pipeline Stages**: Define your Jenkins Pipeline stages in the Jenkinsfile.


## Running the CI/CD Pipeline

1. **Trigger Pipeline**: Make a commit to your GitLab repository to trigger the CI/CD pipeline.
2. **Monitor Execution**: Monitor the pipeline execution in Jenkins to ensure it runs successfully.
3. **Verify Deployment**: Verify that the application is deployed to the test environment as expected.


**Sample Jenkinsfile:**

pipeline {
    agent any
    
    stages {
        stage('Checkout') {
            steps {
                git 'https://gitlab.com/tanviishingi_17/simpleapp.git'
            }
        }
        stage('Build') {
            steps {
                sh 'Application Build'
            }
        }
        stage('Test') {
            steps {
                sh 'Application test'
            }
        }
        stage('Deploy') {
            steps {
                sh 'Application deployed'
            }
        }
    }
}
